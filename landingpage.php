<?php require 'data/games.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="images/brainster.png" type="image/png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/landingpage.css">
    <title>Brainster ToolBox</title>
</head>

<body>


    <?php include 'layout/header.html'; ?>


    <?php 

    echo '<div class="container-fluid bg-up p0">';
        echo '<div class="row m0">';
            echo '<div class="col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1  col-xs-10 col-xs-offset-1 wrap p0">';
            
            foreach ($games as $key => $game) {

                $game_img = $game['game-img'];
                $title = $game['title'];
                $category = $game['category'];
                $name = $game['name'];
                $img = $game['img'];
                $time = $game['time'];
                $text = $game['text'];
                $hour = $game['hour'];

                echo '<div class="col-lg-4 col-md-4 col-sm-4 p0">';
                echo '<div class="clickable">';
                echo "<a href='singlepage.php?gameIndex=$key' target='_blank'>";
                echo '</a>';
                    echo '<div class="card card-box1">';
                        echo "<div class='hidden-xs $game_img'>";

                        echo '</div>';
                        echo '<div class="text">';
                            echo '<div class="left">';
                                echo '<h5>'. $title .'</h5>';
                                echo '<p>'. $category .'<span class="a4">'. $name .'</span></p>';
                            echo '</div>';
                            echo '<div class="right">';
                                echo '<div class="game-logo">';
                                    echo "<img src='$img' class='img-responsive img-rounded' />";
                                echo '</div>';
                            echo '</div>';
                        echo '</div>';
                        echo '<div class="text2">';
                            echo '<span class="time">'. $time .'</i></span>';
                            echo '<span class="time-text">'. $text .'</span>';
                            echo '<p class="hour">'. $hour .'</p>';
                        echo '</div>';
                    echo '</div>';
                echo '</div>';
                echo '</div>';
            }

            echo '</div>';
        echo '</div>';
    echo '</div>';

?>


    <?php include 'layout/footer.html'; ?>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

</body>

</html>