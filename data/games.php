<?php

$games = [
            ['game-img' => 'img1', 'title' => 'Dotmocracy', 'category' => 'Категорија: ', 'name' => 'Акција', 
            'img' => 'images/1.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '5-30 минути'],

            ['game-img' => 'img2', 'title' => 'Project Mid-way Evaluation', 'category' => 'Категорија: ', 'name' => 'Акција', 
            'img' => 'images/2.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '30-60 минути 
            '],

            ['game-img' => 'img3', 'title' => 'Тhe 5 Whys', 'category' => 'Категорија: ', 'name' => 'Иновација', 
            'img' => 'images/3.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '30-60 минути 
            '],

            ['game-img' => 'img4', 'title' => 'Future Trends', 'category' => 'Категорија: ', 'name' => 'Иновација', 
            'img' => 'images/4.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '60-120 минути
            '],

            ['game-img' => 'img5', 'title' => 'Story Building', 'category' => 'Категорија: ', 'name' => 'Иновација', 
            'img' => 'images/5.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '30-60 минути 
            '],

            ['game-img' => 'img6', 'title' => 'Тake a Stand', 'category' => 'Категорија: ', 'name' => 'Иновација', 
            'img' => 'images/6.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '60-120 минути 
            '],

            ['game-img' => 'img7', 'title' => 'IDOARRT Meeting Design', 'category' => 'Категорија: ', 'name' => 'Акција', 
            'img' => 'images/7.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '5-30 минути'],

            ['game-img' => 'img8', 'title' => 'Action Steps', 'category' => 'Категорија: ', 'name' => 'Акција', 
            'img' => 'images/8.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '120-240 минути'],

            ['game-img' => 'img9', 'title' => 'Letter to Myself', 'category' => 'Категорија: ', 'name' => 'Акција', 
            'img' => 'images/9.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '5-30 минути'],

            ['game-img' => 'img10', 'title' => 'Аctive Listening', 'category' => 'Категорија: ', 'name' => 'Лидерство', 
            'img' => 'images/10.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '60-120 минути'],

            ['game-img' => 'img11', 'title' => 'Feedback: I appreciate', 'category' => 'Категорија: ', 'name' => 'Лидерство', 'img' => 'images/11.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '60-120 минути'],

            ['game-img' => 'img12', 'title' => 'Explore your values', 'category' => 'Категорија: ', 'name' => 'Лидерство', 
            'img' => 'images/12.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '60-120 минути'],
            
            ['game-img' => 'img13', 'title' => 'Reflection Individual', 'category' => 'Категорија: ', 'name' => 'Лидерство', 'img' => 'images/13.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '30-60 минути'],

            ['game-img' => 'img14', 'title' => 'Back-turned Feedback', 'category' => 'Категорија: ', 'name' => 'Лидерство',
            'img' => 'images/14.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '60-120 минути'],

            ['game-img' => 'img15', 'title' => 'Conflict Responses', 'category' => 'Категорија: ', 'name' => 'Тим',
            'img' => 'images/15.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '60-120 минути'],

            ['game-img' => 'img16', 'title' => 'Myers-Briggs Team Reflection', 'category' => 'Категорија: ', 'name' => 'Тим',
            'img' => 'images/16.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '60-120 минути'],

            ['game-img' => 'img17', 'title' => 'Personal Presentations', 'category' => 'Категорија: ', 'name' => 'Тим',
            'img' => 'images/17.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '60-240 минути'],

            ['game-img' => 'img18', 'title' => 'Circles of influence', 'category' => 'Категорија: ', 'name' => 'Тим',
            'img' => 'images/18.png', 'time' => '<i class="far fa-clock"></i>', 'text' => 'Времетраење', 'hour' => '30-120 минути']
        ];

?>