<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/forma.css">
    <title>Регистрирај се</title>
</head>

<body>

    <div class="container-fluid p0 m0">
        <div class="row flex m0">
            <div class="col-md-5 col-md-offset-1 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 box">
            
            <?php 
            if(isset($_GET['error'])) {
                if($_GET['error'] == 'required') {
                    echo "<p style='color: red'>You have some empty fields. Please try again.</p>";
                }
                elseif($_GET['error'] == 'invalidemail') {
                echo "<p style='color:red'>The emails do not meet all the requirements. The emails should have at least 5 letters before @.</p>";
                }  elseif($_GET['error'] == 'invalidpassword') {
                echo "<p style='color:red'>The password does not meet all the requirements. The password must have at least one number, a special character and a capital letter.</p>";
                } elseif($_GET['error'] == 'duplicateemail') {
                    echo "<p style='color:red'>There is already a user with that email.</p>";
                } else {
                    echo "<p style='color:red'>An error occured. Please try again later.</p>";    
                }
            }
            if(isset($_GET['success'])) {
                if($_GET['success'] == 'register') {
                    echo "<span style='color:green;'>Registered successfully. Now please login.  </span>";
                    echo '<a href="login.php" style="margin-bottom: 10px" role="button" class="btn btn-default">Login</a>';
                }
            }
            ?>
                <form method="POST" action="formi/registration_forma.php">
                    <div class="form-group form-inline">
                        <label for="firstname">First Name:</label>
                        <input type="text" class="form-control left" name="firstname" placeholder="First Name">
                    </div>
                    <div class="form-group form-inline">
                        <label for="lastname">Last Name:</label>
                        <input type="text" class="form-control left" name="lastname" placeholder="Last Name">
                    </div>
                    <div class="form-group form-inline">
                        <label for="company">Company:</label>
                        <input type="text" class="form-control left" name="company" placeholder="Company">
                    </div>
                    <div class="form-group form-inline">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control left" name="email" placeholder="Email">
                    </div>
                    <div class="form-group form-inline">
                        <label for="pass">Password:</label>
                        <input type="password" class="form-control left" name="pass" placeholder="Password">
                    </div>
                    <div class="form-group form-inline">
                        <label for="phone">Phone Number:</label>
                        <input type="tel" class="form-control left" name="phone" placeholder="Phone Number">
                    </div>
                    <div class="form-group form-inline">
                        <label for="employees">Number of Employees:</label>
                        <select name="employees" class="left">
                            <option value="1">1</option>
                            <option value="2-10">2-10</option>
                            <option value="11-50">11-50</option>
                            <option value="51-200">51-200</option>
                            <option value="200+">200+</option>
                        </select>
                    </div>
                    <div class="form-group form-inline">
                        <label for="department">Department:</label>
                        <select name="department" class="left">
                            <option value="Human resources">Human resources</option>
                            <option value="Marketing">Marketing</option>
                            <option value="Product">Product</option>
                            <option value="Sale">Sale</option>
                            <option value="CEO">CEO</option>
                        </select>
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</body>

</html>