<?php

include 'functions.php'; 

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $company = $_POST['company'];
    $email = $_POST['email'];
    $pass = $_POST['pass'];
    $phone = $_POST['phone'];
    $employees = $_POST['employees'];
    $department = $_POST['department'];
 

    if ($firstname == "" || $lastname == "" || $company == "" || $email == "" || $pass == "" || $phone  == "" || $employees == "" || $department == "") {
        header ("Location: ../registration.php?error=required");
        die (); 
    }
    
    if (!checkEmail($email)) {
        header("Location: ../registration.php?error=invalidemail");
        die();
    }

    if (!checkPassword($pass)) {
        header("Location: ../registration.php?error=invalidpassword");
        die();
    }

    if (!file_exists('formi/Users')) {
        mkdir('formi/Users', 0777, true);
        chmod('formi', 0777);
        chmod('formi/Users', 0777);
    }

    if (!file_exists('Users/users.txt')) {
        file_put_contents("Users/users.txt", "$email=$pass", FILE_APPEND);
    }

    $string = file_get_contents("Users/users.txt");
    $niza = explode("\n", $string);

    foreach ($niza as $row) {
        $e = explode("=",$row)[0];
        if ($e == $email) {
            header ("Location: ../registration.php?error=duplicateemail");
            die();
        }
    }
    
    file_put_contents("Users/users.txt", "$email=$pass\n", FILE_APPEND);

    $folderName = "$firstname$lastname";

    $fileName = "$firstname.txt";

    $data = "$firstname,$lastname,$company,$email,$pass,$phone,$employees,$department";

    if(!file_exists('Users/'.$folderName)) {
        mkdir('Users/'.$folderName, 0777, true);
        chmod('Users', 0777);
        chmod('Users/'.$folderName, 0777);
    }

    $path = "$folderName/$fileName";

    if (!file_exists('Users/'.$path)) {
        file_put_contents('Users/'.$path, $data , FILE_APPEND);
        chmod('Users/'.$folderName, 0777);
        chmod('Users/'.$path, 0777);
    }

    header ("Location: ../registration.php?success=register");
    die();

} else {
    header("Location: ../registration.php?error");
    die();
}