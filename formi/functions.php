<?php

    function checkEmail($email) {
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $array = explode("@", $email);
        $string = $array[0];
        if(preg_match('/^\w{5,}$/', $string)) { // \w equals "[0-9A-Za-z_]"
            // valid email, alphanumeric & longer than or equals 5 chars
            return true;
        }
        return false;
        }
    }

    function checkPassword($password) {
        if(!preg_match("@[0-9]+@", $password)) {
            // no number
            return false;
        }
        if(!preg_match('/[^a-zA-Z\d]/', $password)) {
            // no special character
            return false;
        }
        if(!preg_match("#[A-Z]+#", $password)) {
            // no uppercase
            return false;
        }
        return true;
    }

?>