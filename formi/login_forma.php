<?php

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    
        $email = $_POST['email'];
        $pass = $_POST['pass'];

        if ($email == "" || $pass == "") {
            header ("Location: ../login.php?error=required");
            die (); 
        }

        $users = file_get_contents("Users/users.txt");
        $usersArr = explode("\n",$users);

        echo "<pre>";
        print_r($usersArr);

        $pair = $email."=".$pass;
        $loggedIn = false;
        foreach ($usersArr as $row) {
            if($pair == $row) {
                $loggedIn = true;
                break;
            }
        }

        if($loggedIn) {
            header("Location: ../login.php?success=login");
            die();
        } else {
            header("Location: ../login.php?error=tryagain");
            die();
        }
    }
    else {
        header("Location: ../login.php?error");
        die();
    }
?>