<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="images/brainster.png" type="image/png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/contact_aboutus.css">
    <title>Контакт</title>
</head>

<body>

    <?php include 'layout/header_contact.html'; ?>


    <div class="container-fluid img p0">
        <div class="centered">Контакт</div>
    </div>

    <div class="container-fluid p0">
        <div class="col-md-12 text-center">
            <h3 class="text">Телефон</h3>
            <p>+389 70 38 47 28 </p>
        </div>
    </div>

    <div class="container-fluid p0">
        <div class="col-md-12 text-center">
            <h3 class="text">Емаил</h3>
            <p class="email">contact@brainster.co</p>
        </div>
    </div>


    <?php include 'layout/contact_aboutus.html'; ?>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

</body>

</html>