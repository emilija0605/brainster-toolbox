<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="images/brainster.png" type="image/png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/contact_aboutus.css">
    <title>За нас</title>
</head>

<body>

    <?php include 'layout/header_aboutus.html'; ?>


    <div class="container-fluid img1 p0">
        <div class="centered">Учи, сподели, вмрежи се!</div>
    </div>

    <div class="container-fluid p0">
        <div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 text-center">
            <h3 class="text t1">Ајде заедно да го промениме начинот на кој учиме!</h3>
            <p class="p1">Ние веруваме дека секој поседува практични вештини да предава како и постојана потреба да
                стекнува нови
                знаења.</p>
            <p class="p1">Затоа гo создадовме Brainster.</p>
        </div>
        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1  text-center">
            <p class="p1">Brainster е платформа за офлајн курсеви каде ќе можете да предавате и да посетувате курсеви
                на
                најразлични теми - од курсеви за изработка на мобилни апликации, до курсеви за улична фотографија.</p>
        </div>
    </div>
    <div class="container-fluid p0">
        <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 text-center">
            <h3 class="text t1">Нашата визија е да го претвориме целиот град во универзитет, секој простор во училница
                и секој од нас во инструктор и студент.</h3>
            <p class="p1">Brainster Ви овозможува активно да учествувате во градењето на локалната 2.0 заедница, да се
                вмрежите со
                луѓе со слични интереси, да креирате, да го споделувате Вашето знаење и да учите од другите.</p>
            <p class="p1">Нашата прва станица е Скопје. Очекувајте нѐ во октомври на различни кул локации како
                co-working
                простории, работилници, акселератори, па дури и во кафулиња.</p>
        </div>
    </div>

    <div class="container-fluid p0">
        <div class="col-md-12 text-center">
            <h3 class="text">Контакт</h3>
            <p class="email p1">contact@brainster.co</p>
        </div>
    </div>


    <?php include 'layout/contact_aboutus.html'; ?>


    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>

</body>

</html>