<?php require 'data/singlegame.php'; ?>
<?php

    if(isset($_GET['gameIndex']) && isset($game[$_GET['gameIndex']])) {
        $gameIndex = $_GET['gameIndex'];
        $game1 = $game[$gameIndex];
        } else {
        header("Location: landingpage.php");
        die();
    }

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="images/brainster.png" type="image/png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/singlepage.css">
    <title>Single Page</title>
</head>

<body>

    <?php include 'layout/header_singlepage.html' ?>

    <div class="container-fluid p0">

    <?php 

        $counter = 0;

        foreach ($game1 as $key => $game) {
        
        $counter++;

        $title = $game1['title'];  
        $time = $game1['time'];      
        $size = $game1['size'];
        $level = $game1['level'];
        $materials = $game1['materials'];
        $text = $game1['text'];
        $steps = $game1['steps'];
        
        echo '<div class="row m0">';
            echo '<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">';
                echo '<h1 class="name_of_game">'. $title .'</h1>';
            echo '</div>';
        echo '</div>';

        echo '<div class="row m0">';
            echo '<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 gray text-center">';
                echo '<div class="col-md-3 col-sm-6 mobile">';
                    echo '<span class="gray-icon"><i class="far fa-clock"></i></span>';
                        echo '<span class="gray-text">Времетраење</span>';
                        echo '<p>'. $time .'</p>';
            echo '</div>';
            echo '<div class="col-md-3 col-sm-6 mobile">';
                echo '<span class="gray-icon"><i class="fas fa-user-friends"></i></span>';
                echo '<span class="gray-text">Големина на група</span>';
                echo '<p>'. $size .'</p>';
            echo '</div>';
            echo '<div class="col-md-3 col-sm-6 tablet mobile">';
                echo '<span class="gray-icon"><i class="fas fa-university"></i></span>';
                echo '<span class="gray-text">Ниво за фасилитирање</span>';
                echo '<p>'. $level .'</p>';
            echo '</div>';
            echo '<div class="col-md-3 col-sm-6 tablet mobile">';
                echo '<span class="gray-icon"><i class="fas fa-paint-roller"></i></span>';
                echo '<span class="gray-text">Материјали</span>';

                foreach ($materials as $value) {
                    echo "<p>$value</p>";
                }
            
            echo '</div>';
        echo '</div>';
    echo '</div>';

    echo '<div class="row m0">';
        echo '<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 g-text">';
            echo '<p>'. $text .'</p>';
            echo '<hr>';
        echo '</div>';
    echo '</div>';

    echo '<div class="row m0">';

        $i = 1;

        foreach ($steps as $key => $arr) {
            echo '<div class="col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1 g-text">';
            
            echo '<h4>Чекор '. $i .':</h4>'; 
            $i++;
            
            foreach ($arr as $value) {
                echo $value;
            }
            
            if ($key != count($steps)-1) {
                echo '<hr>';
            }
            echo '</div>';
        }
    echo '</div>';

    if ($counter == 1 ) {
        break;
    }

    }

    ?>

    </div>

    <?php include 'layout/footer_singlepage.html' ?>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
    <script>
        /*Detect scrollTop */
        $(window).scroll(function () {
            if ($(window).scrollTop() > 0) {
                $('.box-top').css('background', 'rgba(77, 38, 146, 0.9)');
            } else {
                $('.box-top').css('background', 'transparent');
            }
        })
    </script>
</body>

</html>