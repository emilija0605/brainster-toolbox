<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.0/css/all.css" integrity="sha384-aOkxzJ5uQz7WBObEZcHvV5JvRW3TUc2rNPA7pe3AwnsUohiw1Vj2Rgx2KSOkF5+h"
        crossorigin="anonymous">
    <link rel="stylesheet" href="css/forma.css">
    <title>Најави се</title>
</head>

<body class="height">

    <div class="container-fluid p0 m0">
        <div class="row flex m0">
            <div class="col-md-4 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 box">
            
            <?php
                if(isset($_GET['error'])) {
                    if($_GET['error'] == 'required') {
                        echo "<p style='color: red'>You have some empty fields. Please try again.</p>";
                    } else if($_GET['error'] == 'tryagain') {
                        echo "<p style='color: red'>Wrong information. A user with that pair of emails and passwords was not found.</p>";  
                    } else {
                        echo "<p style='color:red'>An error occured. Please try again later.</p>";    
                    }
                }
                if(isset($_GET['success'])) {
                    if($_GET['success'] == 'login') {
                        echo "<p style='color:green;'>Logged in successfully!</p>";
                    }
                }
            ?>
                <form method="POST" action="formi/login_forma.php">
                    <div class="form-group">
                        <label for="email">Email:</label>
                        <input type="email" class="form-control" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="pass">Password:</label>
                        <input type="password" class="form-control" name="pass" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                </form>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous">
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
</body>

</html>